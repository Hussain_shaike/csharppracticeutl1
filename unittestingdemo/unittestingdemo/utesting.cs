﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace unittestingdemo
{
    class utesting
    {
        ////unit test for code reuse.
     //copy paste this code file to build new unit tests quickly.
        [TestClass]
        public class SampleTest
        {
            [TestMethod]
            public void Testsample1()
            {
                var actual = true;
                var expected = true;
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void Testsample2()
            {
                var actual = true;
                var expected = false;
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void Testsample3()
            {
                var input1 = 52574874899.8;
                var input2 = 105656855.2;
                var actual = 52680531755;
                var expected = Program.SumOfTwoNumbers(input1,input2);
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void Testsample4()
            {
                var input1 = 210.36;
                var input2 = 310.28;
                var actual = Program.SumOfTwoNumbers(input1, input2); 0
                var expected = 520.64;
                Assert.AreEqual(expected, actual);
            }
        }
      
    }
}
