﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(showlist.Startup))]
namespace showlist
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
